//
//  Client+Idea.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import FluentMySQL
import Vapor


final class ClientIdea: MySQLModel {
    var id: Int?
    var idea:Int
    var client: Int
    var contact: Int?
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, idea:Int, client: Int, contact:Int?) {
        self.id = id
        self.idea = idea
        self.client = client
        self.contact = contact
        
    }
    
    
}


extension ClientIdea: Content {}
extension ClientIdea: Migration {}
extension ClientIdea {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension ClientIdea: Parameter {}
