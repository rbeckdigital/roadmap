//
//  AttributeType.swift
//  App
//
//  Created by Richard Beck on 2019-02-01.
//

import Foundation
import FluentMySQL
import Vapor


final class AttributeType: MySQLModel {
    var id: Int?
    var name: String
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, name: String ) {
        self.id = id
        self.name = name

        
    }
    
    
}


extension AttributeType: Content {}
extension AttributeType: Migration {}
extension AttributeType {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension AttributeType: Parameter {}

extension AttributeType {
    // this galaxy's related planets
    var attributeTypes: Children<AttributeType, Attribute> {
        return children(\.attributeTypeId)
    }
}

extension Attribute {
    // this planet's related galaxy
    var attributes: Parent<Attribute, AttributeType> {
        return parent(\.attributeTypeId)
    }
}
