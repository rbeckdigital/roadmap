//
//  ideaOverview.swift
//  App
//
//  Created by Richard Beck on 2019-02-09.
//

import Foundation
import Fluent
import Vapor


struct IdeaOverview: Content {
    var id: Int?
    var title: String
    var description: String
    var status: String
    var creatorUserId: Int
    var createdAt: Date?
    var updatedAt: Date?
    var commercial:Double?
    var market:Double?
    var efficiency:Double?
    var customerValue:Double?
    var customers:Int?
    var creator:String?
    var businessValue:Double?
    
    init(idea:Idea, user:User?, businessValue:[Attribute]?) {
        self.id = idea.id
        self.title = idea.title
        self.description = idea.description
        self.status = idea.status
        self.creatorUserId = idea.creatorUserId
        self.creator = user?.email
        self.createdAt = idea.createdAt
        self.updatedAt = idea.updatedAt
        self.commercial = AttributeController().getParticularAttribute(attributeList:businessValue, type:3)
        self.market = AttributeController().getParticularAttribute(attributeList:businessValue, type:4)
        self.efficiency = AttributeController().getParticularAttribute(attributeList:businessValue, type:5)
        self.customerValue = AttributeController().getParticularAttribute(attributeList:businessValue, type:6)
        self.businessValue = (self.commercial ?? 0)+(self.market ?? 0)
        self.businessValue = self.businessValue!+(self.efficiency ?? 0)+(self.customerValue ?? 0)
        
        print(businessValue)
    }
    
    
}
