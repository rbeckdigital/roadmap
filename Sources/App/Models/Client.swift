//
//  Client.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import FluentMySQL
import Vapor


final class Client: MySQLModel {
    var id: Int?
    var name: String
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, name: String) {
        self.id = id
        self.name = name
        
    }
    
    
}


extension Client: Content {}
extension Client: Migration {}
extension Client {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Client: Parameter {}
