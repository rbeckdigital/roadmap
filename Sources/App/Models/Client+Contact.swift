//
//  Client+Idea.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import FluentMySQL
import Vapor


final class ClientContact: MySQLModel {
    var id: Int?
    var client: Int
    var contact: Int
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, client: Int, contact:Int) {
        self.id = id
        self.client = client
        self.contact = contact
        
    }
    
    
}


extension ClientContact: Content {}
extension ClientContact: Migration {}
extension ClientContact {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension ClientContact: Parameter {}
