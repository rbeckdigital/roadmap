//
//  Attribute.swift
//  App
//
//  Created by Richard Beck on 2019-02-01.
//

import Foundation
import FluentMySQL
import Vapor


final class Attribute: MySQLModel {
    var id: Int?
    var ideaId: Int
    var attributeTypeId: Int
    var description: String?
    var attributeAmount:Double?
    var creatorUserId:Int
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int? = nil, ideaId: Int, attributeTypeId: Int, description:String, attributeAmount:Double?, creatorUserId:Int) {
        self.id = id
        self.ideaId = ideaId
        self.attributeTypeId = attributeTypeId
        self.description = description
        self.creatorUserId = creatorUserId
        self.attributeAmount = attributeAmount
        
    }
    
    
}


extension Attribute: Content {}
extension Attribute: Migration {}
extension Attribute {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Attribute: Parameter {}

extension Idea {
    // this galaxy's related planets
    var attributes: Children<Idea, Attribute> {
        return children(\.ideaId)
    }
}

extension Attribute {
    // this planet's related galaxy
    var idea: Parent<Attribute, Idea> {
        return parent(\.ideaId)
    }
}
