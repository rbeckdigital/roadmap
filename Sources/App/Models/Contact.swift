//
//  Contact.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import FluentMySQL
import Vapor


final class Contact: MySQLModel {
    var id: Int?
    var name: String
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, name: String) {
        self.id = id
        self.name = name
        
    }
    
    
}


extension Contact: Content {}
extension Contact: Migration {}
extension Contact {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Contact: Parameter {}
