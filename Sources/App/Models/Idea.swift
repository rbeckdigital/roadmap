//
//  Idea.swift
//  App
//
//  Created by Richard on 2019-02-01.
//

import Foundation
import FluentMySQL
import Vapor


final class Idea: MySQLModel {
    var id: Int?
    var title: String  // added
    var description: String
    var status: String
    var creatorUserId: Int
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, title: String, description: String, status:String, creatorUserId:Int) {
        self.id = id
        self.title = title
        self.description = description
        self.status = status
        self.creatorUserId = creatorUserId
        
    }
    
    
}


extension Idea: Content {}
extension Idea: Migration {}
extension Idea {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Idea: Parameter {}
