import Vapor
import Authentication

/// Register your application's routes here.
public func routes(_ router: Router) throws {

    // Security //
    let userController = UserController()
    router.post("setup", use: userController.setUp)
    
    
    let authSessionRouter = router.grouped(User.authSessionsMiddleware())
    let auth = authSessionRouter.grouped(RedirectMiddleware<User>(path: "/login"))
    authSessionRouter.post("login", use: userController.login)
    router.get("logout", use: userController.logout)
    authSessionRouter.post("tokenLogin", use: userController.tokenLogin)
    auth.post("register", use: userController.register)
    auth.get("profile", use: userController.profile)
    authSessionRouter.get("authStatus", use: userController.authStatus)
    
    // BackEnd //
    let ideaController = IdeaController()
    auth.post("idea", use: ideaController.create)
    auth.get("idea","list", use: ideaController.list)
    
    let attributeController = AttributeController()
    auth.post("idea","attribute", use: attributeController.create)
    auth.post("attribute","type", use: attributeController.createType)
    
    
    let clientController = ClientController()
    auth.get("client","list", use: clientController.list)
    auth.post("client", use: clientController.create)
    auth.patch("client",Int.parameter, use: clientController.update)
    auth.delete("client",Client.parameter, use: clientController.delete)
    
    let contactController = ContactController()
    auth.get("contact","list", use: contactController.list)
    auth.post("contact", use: contactController.create)
    auth.patch("contact",Int.parameter, use: contactController.update)
    auth.delete("contact",Contact.parameter, use: contactController.delete)
    
    
    let clientContactController = ClientContactController()
    auth.get("client","contact","list", use: clientContactController.list)
    auth.post("client","contact", use: clientContactController.create)
    auth.patch("client","contact",Int.parameter, use: clientContactController.update)
    auth.delete("client","contact",ClientContact.parameter, use: clientContactController.delete)
    
    let clientIdeaController = ClientIdeaController()
    auth.get("idea","client","list", use: clientIdeaController.list)
    auth.post("idea","client", use: clientIdeaController.create)
    auth.patch("idea","client",Int.parameter, use: clientIdeaController.update)
    auth.delete("idea","client",ClientIdea.parameter, use: clientIdeaController.delete)
    
    // UI //
    let uiUserController = UIUserController()
    router.get("login", use: uiUserController.renderLogin)
    
    let uiIdeaController = UIIdeaController()
    auth.get("idea","list", use: uiIdeaController.list)
    /*
    auth.get("idea","table", use: uiIdeaController.table)
*/
    router.get("idea","table","data", use: uiIdeaController.tabledata)
 
    router.get("idea","table","row",Int.parameter, use: uiIdeaController.tablerow)
}
