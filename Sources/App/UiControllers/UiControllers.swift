//
//  UiControllers.swift
//  App
//
//  Created by Richard Beck on 2019-02-08.
//

import Foundation
import Vapor
import Fluent


final class UIUserController {
    
    func renderLogin(_ req: Request) throws -> Future<View> {
        return try req.view().render("login")
}



}
