//
//  UiIdeaController.swift
//  App
//
//  Created by Richard Beck on 2019-02-08.
//

import Foundation
import Vapor
import Fluent


final class UIIdeaController {
    
    func list(_ req: Request) throws -> Future<View> {
        return try req.view().render("ideas")
    }
    
    /*
    func table(_ req: Request) throws -> Future<View> {
        return Idea.query(on: req).all()
            .flatMap {ideas in  ideas
            .flatMap {idea in  idea
            .flatMap {user in creator

            let data = ["ideas":ideas]
            return try req.view().render("ideasTable",data)
        }
        }
    }
    */
        
    func tablerow(req: Request) throws -> Future<IdeaOverview> {
        let parameter = try req.parameters.next(Int.self)
        return Idea.find(parameter, on: req)
            .flatMap { idea in
                if idea == nil {throw Abort(.notFound)}
                return User.find(((idea?.creatorUserId)!), on:req)
            
            .flatMap { user in
                        if user == nil {throw Abort(.notFound)}
                        return Attribute.query(on: req).filter(\.ideaId == idea!.id!).all()
            .map { businessValue in
                IdeaOverview(idea:idea!, user:user!, businessValue:businessValue)
                        
                }}}}
    
    
    func tabledata(req: Request) throws -> Future<[IdeaOverview]> {
        return Idea.query(on: req).all()
            .map { ideas in ideas
                
            .flatMap { idea in
                User.find(idea.creatorUserId, on:req)
        
            .map { user in
                
                 IdeaOverview(idea:idea, user:user, businessValue:nil)
                                
                }}}}
    
    
}





/*
 let ideaOverview = ["title":idea.title,
 "description":idea.description,
 "status":idea.status,
 "creator":try UserController().user(req,userId: idea.creatorUserId).unwrap(or: Abort(.notFound)) ] as [String : Any]
 print(ideaOverview)
 */
