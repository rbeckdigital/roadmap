//
//  ContactController.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import Vapor
import Fluent


final class ClientContactController {
    
    func list(_ req: Request) throws -> Future<[ClientContact]> {
        return ClientContact.query(on: req).all()
    }
    
    
    func create(_ req: Request) throws -> Future<ClientContact> {
        return try req.content.decode(ClientContact.self).flatMap { clientContact in
            return clientContact.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(ClientContact.self).flatMap { clientContact in
            return clientContact.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<ClientContact> {
        return ClientContact.find(try req.parameters.next(Int.self), on: req).flatMap { clientContact in
            if clientContact == nil {throw Abort(.notFound)}
            return try req.content.decode(ClientContact.self).flatMap { newClientContact in
                clientContact?.client = newClientContact.client
                clientContact?.contact = newClientContact.contact
                return (clientContact?.save(on: req))!
            }
        }
    }
    
    
}
