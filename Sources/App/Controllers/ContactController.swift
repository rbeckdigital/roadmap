//
//  ContactController.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import Vapor
import Fluent


final class ContactController {
    
    func list(_ req: Request) throws -> Future<[Contact]> {
        return Contact.query(on: req).all()
    }
    
    
    func create(_ req: Request) throws -> Future<Contact> {
        return try req.content.decode(Contact.self).flatMap { contact in
            return contact.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Contact.self).flatMap { contact in
            return contact.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Contact> {
        return Contact.find(try req.parameters.next(Int.self), on: req).flatMap { contact in
            if contact == nil {throw Abort(.notFound)}
            return try req.content.decode(Contact.self).flatMap { newContact in
                contact?.name = newContact.name
                return (contact?.save(on: req))!
            }
        }
    }
    
    
}
