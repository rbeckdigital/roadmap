//
//  ClientController.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import Vapor
import Fluent


final class ClientController {
    
    func list(_ req: Request) throws -> Future<[Client]> {
        return Client.query(on: req).all()
    }
    
    
    func create(_ req: Request) throws -> Future<Client> {
        return try req.content.decode(Client.self).flatMap { client in
            return client.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Client.self).flatMap { client in
            return client.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Client> {
        return Client.find(try req.parameters.next(Int.self), on: req).flatMap { client in
            if client == nil {throw Abort(.notFound)}
            return try req.content.decode(Client.self).flatMap { newClient in
                client?.name = newClient.name
                return (client?.save(on: req))!
            }
        }
    }
    
    
}
