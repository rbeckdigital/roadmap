//
//  ClientIdea.swift
//  App
//
//  Created by Richard Beck on 2019-02-07.
//

import Foundation
import Vapor
import Fluent


final class ClientIdeaController {
    
    func list(_ req: Request) throws -> Future<[ClientIdea]> {
        return ClientIdea.query(on: req).all()
    }
    
    
    func create(_ req: Request) throws -> Future<ClientIdea> {
        return try req.content.decode(ClientIdea.self).flatMap { clientIdea in
            return clientIdea.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(ClientIdea.self).flatMap { client in
            return client.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<ClientIdea> {
        return ClientIdea.find(try req.parameters.next(Int.self), on: req).flatMap { clientIdea in
            if clientIdea == nil {throw Abort(.notFound)}
            return try req.content.decode(ClientIdea.self).flatMap { newClientIdea in
                clientIdea?.idea = newClientIdea.idea
                clientIdea?.client = newClientIdea.client
                clientIdea?.contact = newClientIdea.contact
                return (clientIdea?.save(on: req))!
            }
        }
    }
    
    
}
