//
//  IdeaController.swift
//  App
//
//  Created by Richard Beck on 2019-02-01.
//

import Foundation
import Vapor
import Fluent


final class IdeaController {
    
    func list(_ req: Request) throws -> Future<[Idea]> {
        return Idea.query(on: req).all()
    }
    
    
    func create(_ req: Request) throws -> Future<Idea> {
        return try req.content.decode(Idea.self).flatMap { idea in
            return idea.save(on: req)
        }
    }
  
}

