//
//  UserController.swift
//  App
//
//  Created by Richard on 2019-02-01.
//

import Foundation
import Vapor
import Crypto

final class UserController {
    
    func setUp(_ req: Request) throws -> Future<Future<User.Public>> {
        
        return User.query(on: req).first().map() {
            user -> EventLoopFuture<User.Public> in
            print("user")
            print(user)
            
            if  user != nil {
                throw Abort(.unauthorized)
            } else {
                return try self.register(req)
            }
            
        }

    }
    
    
    func register(_ req: Request) throws -> Future<User.Public> {
        return try req.content.decode(User.self).flatMap { user in
            let hasher = try req.make(BCryptDigest.self)
            let passwordHashed = try hasher.hash(user.password)
            let newUser = User(email: user.email, password: passwordHashed)
            return newUser.save(on: req).map { storedUser in
                return User.Public(
                    id: try storedUser.requireID(),
                    email: storedUser.email
                )
            }
        }
    }
    
    
    func login(_ req: Request) throws -> Future<User.Public> {
        return try req.content.decode(User.self).flatMap { user in
            return User.authenticate(
                username: user.email,
                password: user.password,
                using: BCryptDigest(),
                on: req
                ).map { user in
                    guard let user = user else {
                        throw Abort(.unauthorized)
                    }
                    try req.authenticateSession(user)
                    try req.session()["logged"] = "true"
                    return User.Public(id:try user.requireID(), email:user.email)
            }
        }
    }
    
    
    
    func tokenLogin(_ req: Request) throws -> Future<Int> {
        return try req.content.decode(User.self).flatMap { user in
            return User.authenticate(
                username: user.email,
                password: user.password,
                using: BCryptDigest(),
                on: req
                ).map { user in
                    guard let user = user else {
                        throw Abort(.unauthorized)
                    }
                    try req.authenticateSession(user)
                    try req.session()["logged"] = "true"
                    print(UserToken.self)
                    return UserToken.tokenKey.hashValue
            }
        }
    }
    
    
    
    
    func profile(_ req: Request) throws -> String {
        let user = try req.requireAuthenticated(User.self)
        return "You're viewing \(user.email) profile."
    }
    
    func logout(_ req: Request) throws -> Future<String> {
        try req.unauthenticateSession(User.self)
        return Future.map(on: req) { return "true" }
    }
    
    func authStatus(_ req: Request) throws -> Future<String> {
        return req.future("{\"status\":\(try req.isAuthenticated(User.self))}")
    }
    
    func user(_ req:Request, userId:Int) throws -> Future<User?> {
        return User.find(userId, on:req)
    }


}
