//
//  AttributeController.swift
//  App
//
//  Created by Richard Beck on 2019-02-01.
//

import Foundation
import Vapor
import Fluent


final class AttributeController {
    
    func list(_ req: Request) throws -> Future<[Idea]> {
        return Idea.query(on: req).all()
    }
    
    
    func create(_ req: Request) throws -> Future<Attribute> {
        return try req.content.decode(Attribute.self).flatMap { attribute in
            return attribute.save(on: req)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Attribute.self).flatMap { attribute in
            return attribute.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Attribute> {
        return Attribute.find(try req.parameters.next(Int.self), on: req).flatMap { attribute in
            if attribute == nil {throw Abort(.notFound)}
            return try req.content.decode(Attribute.self).flatMap { newAttribute in
                attribute?.ideaId = newAttribute.ideaId
                attribute?.description = newAttribute.description
                attribute?.attributeTypeId = newAttribute.attributeTypeId
                attribute?.creatorUserId = newAttribute.creatorUserId
                attribute?.attributeAmount = newAttribute.attributeAmount
                return (attribute?.save(on: req))!
            }
        }
    }
    
    
    func createType(_ req: Request) throws -> Future<AttributeType> {
        return try req.content.decode(AttributeType.self).flatMap { attributeType in
            return attributeType.save(on: req)
        }
    }
    
    func getParticularAttribute(attributeList:[Attribute]?, type:Int) -> Double? {
        
        
        for attribute in attributeList ?? [] {
            if attribute.attributeTypeId == type {
                return attribute.attributeAmount
            }
        }
        return nil
    }
    
}

