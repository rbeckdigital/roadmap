
<script>







function getAuthStatus() {
    
    var authStatus = "false";
    
    $.ajax
    ({
     
     url: '/authStatus',
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){ var data = JSON.parse(result); authStatus = data.status; console.log(authStatus); editMode(authStatus);},
     error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
     
     });
    
    return authStatus;
}

if (document.readyState === "interactive") {console.log("DOM Complete"); getAuthStatus(); }
else {window.onload = getAuthStatus();}










function logout() {
    
    $.ajax
    ({
     url: '/logout',
     type: 'get',
     success: function(result){
     if(result == "true") {location.reload(true)};
     
     },
     
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    
}



</script>
